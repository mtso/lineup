package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

var (
	port = os.Getenv("PORT")
)

func main() {
	if port == "" {
		log.Fatalf("Need to provide PORT string [%s]", port)
	}

	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprintf(w, "Hello, world")
	})

	fmt.Printf("Listening on %s\n", port)
	err := http.ListenAndServe(fmt.Sprintf(":%s", port), mux)
	fmt.Println(err)
}
