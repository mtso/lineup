.PHONY: build web

target_dir := target

build:
	go build -o $target_dir/$svc_name cmd/server.go

web:
	make -C $weather-web
